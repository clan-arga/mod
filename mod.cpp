name = "Mod ArgA";
version = "1.2.0";
author = "Equipo de Mod ArgA";
description = "Mod ArgA - Versión 1.2.0";

overview = "Mod ArgA es una modificación creada por el Clan ArgA de simulacion táctica, con el esfuerzo de todo su equipo, para cumplir la meta de jugar con su propio sello distintivo dentro de ArmA3";

overviewPicture = "mod.paa";
overviewText = "Mod oficial del Clan ArgA";

actionName = "Sitio web";
action = "http://www.clanarga.com.ar";

tooltip = "Mod ArgA"; 
picture = "logo.paa";
logo = "logo.paa";